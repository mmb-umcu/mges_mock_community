PASS	Basic Statistics	ERR2984773_1.fastq
PASS	Per base sequence quality	ERR2984773_1.fastq
PASS	Per tile sequence quality	ERR2984773_1.fastq
PASS	Per sequence quality scores	ERR2984773_1.fastq
WARN	Per base sequence content	ERR2984773_1.fastq
FAIL	Per sequence GC content	ERR2984773_1.fastq
PASS	Per base N content	ERR2984773_1.fastq
PASS	Sequence Length Distribution	ERR2984773_1.fastq
PASS	Sequence Duplication Levels	ERR2984773_1.fastq
PASS	Overrepresented sequences	ERR2984773_1.fastq
FAIL	Adapter Content	ERR2984773_1.fastq
PASS	Basic Statistics	ERR2984773_2.fastq
PASS	Per base sequence quality	ERR2984773_2.fastq
PASS	Per tile sequence quality	ERR2984773_2.fastq
PASS	Per sequence quality scores	ERR2984773_2.fastq
WARN	Per base sequence content	ERR2984773_2.fastq
FAIL	Per sequence GC content	ERR2984773_2.fastq
PASS	Per base N content	ERR2984773_2.fastq
PASS	Sequence Length Distribution	ERR2984773_2.fastq
PASS	Sequence Duplication Levels	ERR2984773_2.fastq
PASS	Overrepresented sequences	ERR2984773_2.fastq
FAIL	Adapter Content	ERR2984773_2.fastq
