#!/bin/bash
source /home/dla_mm/eklijn/data/miniconda3/etc/profile.d/conda.sh
conda activate mock_community
#$ -S /bin/bash
#$ -cwd
#$ -M e.v.klijn-3@umcutrecht.nl
#$ -m beas
# Request resources (64GB, 8 threads and 8 wallclock-hours)
#$ -l h_vmem=64G
#$ -l h_rt=8:00:00
#$ -pe threaded 8
#Logs
#$ -o ./output_messages/assembly_output.txt
#$ -e ./error_messages/assembly_error.txt

spades.py --meta -1 ../data/trimmomatic/ERR2984773_1.trimmed.fastq -2 ../data/trimmomatic/ERR2984773_2.trimmed.fastq -o ../metaSPAdes/
